package com.amou.contact3

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_update_db.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.info
import org.jetbrains.anko.longToast
import java.net.URL

class UpdateDbActivity : AppCompatActivity() ,AnkoLogger{
    lateinit var contacts : MutableList<Contacts>
    var id : Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_db)
        var jsonurl = "http://www.kingbright.com:9999/contact.service"
        doAsync {
            val json = URL(jsonurl).readText()
            contacts = Gson().fromJson<MutableList<Contacts>>(json,
                object : TypeToken<MutableList<Contacts>>() {}.type
            )
        }

        button_updatedb.setOnClickListener {

            updateDB()

        }
    }

    private fun updateDB() {
        val helper = ContactHelper(this, "contactlist", null, 1)
        val values = ContentValues()
        //將table 資料清除
        helper.writableDatabase
            .delete("contact", "", null)
        //用迴圈將資料寫入到 table
        for (contact in contacts) {
            values.put("business_phone", contact.business_phone)
            values.put("combo_name", contact.combo_name)
            values.put("email", contact.email)
            values.put("emp_code", contact.emp_code)
            values.put("mobile", contact.mobile)
            values.put("name", contact.name)
            id = helper.writableDatabase
                .insert("contact", null, values)

            info("${contact.name}")
        }

        if (id > -1) {
            longToast("新增完成")
        } else {
            longToast("新增失敗")
        }
    }
}
