package com.amou.contact3

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_contact_list.*
import kotlinx.android.synthetic.main.content_contact_list.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.row_contactlist.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.info
import org.jetbrains.anko.uiThread
import java.net.URL

class ContactListActivity : AppCompatActivity(), AnkoLogger {

    lateinit var contacts: MutableList<Contacts>
    lateinit var searchContacts: MutableList<Contacts>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_list)
        setSupportActionBar(toolbar)


        val combo = intent.getIntExtra("combo", -1)

        var combourl = ""
        var comboTitle = ""
        when (combo) {
            100 -> {
                combourl = "f"
                comboTitle = "國外營業部"
            }

            200 -> {
                combourl = "d"
                comboTitle = "國內營業部"
            }

            300 -> {
                combourl = "m"
                comboTitle = "管理部"
            }
            400 -> {
                combourl = "a"
                comboTitle = "會計部"
            }
            500 -> {
                combourl = "p"
                comboTitle = "企劃部"
            }
            600 -> {
                combourl = "j"
                comboTitle = "日本事業部"
            }
            700 -> {
                combourl = "t"
                comboTitle = "台幹"
            }
        }
        info(comboTitle)
        setTitle(comboTitle)

        var jsonurl = "http://www.kingbright.com:9999/contact.combo?c=" + combourl
        info (jsonurl )
        //產生recyclerView 分隔線物件
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recycler_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ContactListActivity)
            //新增分隔線
            addItemDecoration(itemDecoration)
        }

        doAsync {
            val json = URL(jsonurl).readText()
            contacts = Gson().fromJson<MutableList<Contacts>>(json,
                object : TypeToken<MutableList<Contacts>>() {}.type
            )
            searchContacts = Gson().fromJson<MutableList<Contacts>>(json,
                object : TypeToken<MutableList<Contacts>>() {}.type
            )
            contacts.forEach {
                info("${it.name}")
            }

            uiThread {
                recycler_list.adapter = ContactAdapter(contacts)

            }

        }
    }

    inner class ContactAdapter(val contactApi: MutableList<Contacts>) : RecyclerView.Adapter<ContactHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_contactlist, parent, false)
            return ContactHolder(view)
        }

        override fun getItemCount(): Int {
            return contactApi.size
        }

        override fun onBindViewHolder(holder: ContactHolder, position: Int) {
            holder.nameText.text = contactApi.get(position).name
            //holder.extText.text =contactApi.get(position).business_phone
            //holder.mobileText.text= contactApi.get(position).mobile
            holder.itemView.setOnClickListener {
                functionClicked(contactApi.get(position), position)
            }
        }

    }

    private fun functionClicked(contacts: Contacts, position: Int) {
        startActivityForResult(
            Intent(this, ContactSingleActivity::class.java)
                .putExtra("selectuser", contacts), Activity.RESULT_OK
        )
    }

    inner class ContactHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameText: TextView = view.text_sname
        //val extText: TextView =view.text_ext
        //val mobileText: TextView=view.text_mobile
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_contactlist, menu)
        val searchViewItem = menu?.findItem(R.id.action_search2)
        val searchView = searchViewItem?.actionView as SearchView
        //searchview 打開時 能佔滿toolbar
        searchView.maxWidth = Int.MAX_VALUE
        searchView.queryHint = "Search View Hint"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            //當搜尋匡內容變動即觸發
            override fun onQueryTextChange(newText: String?): Boolean {
                //text_tvEmpty.visibility = View.GONE

                //搜尋匡內容為空時 顯示原本的內容的recyclerview
                if (newText!!.isEmpty()) {
                    recycler_list.adapter = ContactAdapter(contacts)
                } else {
                    //清空搜尋物件
                    searchContacts.clear()
                    //用迴圈撈出物件資料 比對與搜尋匡輸入內容是否相符 相符即新增到搜尋用物件中
                    for (contact in contacts) {
                        if (contact.business_phone.toLowerCase().contains(newText.toLowerCase())) {
                            searchContacts.add(contact)
                        }
                        if (contact.name.toLowerCase().contains(newText.toLowerCase())) {
                            searchContacts.add(contact)
                        }
                        if (contact.email.toLowerCase().contains(newText.toLowerCase())) {
                            searchContacts.add(contact)
                        }
                    }
                    /*if(searchContacts.isEmpty()){
                        text_tvEmpty.visibility = View.VISIBLE
                    }*/
                    recycler_list.adapter = ContactAdapter(searchContacts)
                }
                return false

            }

        })
        return true
    }
}


