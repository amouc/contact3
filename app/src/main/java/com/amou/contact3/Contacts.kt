package com.amou.contact3

import android.os.Parcel
import android.os.Parcelable


class Contacts(
    var business_phone: String,
    var combo_name: String,
    var email: String,
    var emp_code: String,
    var mobile: String,
    var name: String,
    val title: String
//繼承 Parcelable 介面 並實作他的方法 用來在intent 傳遞物件
):Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(business_phone)
        parcel.writeString(combo_name)
        parcel.writeString(email)
        parcel.writeString(emp_code)
        parcel.writeString(mobile)
        parcel.writeString(name)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Contacts> {
        override fun createFromParcel(parcel: Parcel): Contacts {
            return Contacts(parcel)
        }

        override fun newArray(size: Int): Array<Contacts?> {
            return arrayOfNulls(size)
        }
    }
}