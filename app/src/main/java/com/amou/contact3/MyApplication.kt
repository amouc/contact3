package com.amou.contact3

import android.app.Application
import android.content.Context
import android.util.Log


class MyApplication : Application() {
    var TAG = "MainActivityMyApp"
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate7: ")
        getSharedPreferences("contact", Context.MODE_PRIVATE)
            .apply {
                val name = getString("name","jj")
                val mobille = getString("mobile","null")
                val email = getString("email","null")
            }
    }
}