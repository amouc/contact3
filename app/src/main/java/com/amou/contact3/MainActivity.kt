package com.amou.contact3

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_contact_list.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.row_contact.view.*
import kotlinx.android.synthetic.main.row_contactlist.view.*
import org.jetbrains.anko.*
import java.net.URL

class MainActivity : AppCompatActivity(), AnkoLogger {


    val foreign = 100
    val domestic = 200
    val management = 300
    val account = 400
    val planning = 500
    val japan = 600
    val tw = 700
    val combos = listOf("國外營業部", "國內營業部", "管理部", "會計部", "企劃部", "日本事業部", "台幹")

    //先產生搜尋用的物件
    lateinit var contacts: MutableList<Contacts>
    lateinit var searchContacts: MutableList<Contacts>
    lateinit var contact: Contacts
    lateinit var searchContact: Contacts
    var id: Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //讀資料庫資料進來
        val helper = ContactHelper(this)
        //(table名稱,要查詢的欄位,where語法,where參數,groupby語法,having語法,orderBy排序)
        val cursor = helper.readableDatabase.query(
            "contact",
            null, null, null, null, null, null
        )
        //初始化物件
        contacts = mutableListOf<Contacts>()
        searchContacts = mutableListOf<Contacts>()
        //檢查ＤＢ有無資料
        if (cursor.count > 0) {
            //先移動cursor游標到開頭位置
            cursor.moveToFirst()
            for (i in 0 until cursor.count) {
                //初始化物件
                contact = Contacts("", "", "", "", "", "", "")
                //取得資料 拿欄位名稱換index
                contact.business_phone = cursor.getString(cursor.getColumnIndex("business_phone"))
                contact.combo_name = cursor.getString(cursor.getColumnIndex("combo_name"))
                contact.email = cursor.getString(cursor.getColumnIndex("email"))
                contact.emp_code = cursor.getString(cursor.getColumnIndex("emp_code"))
                contact.mobile = cursor.getString(cursor.getColumnIndex("mobile"))
                contact.name = cursor.getString(cursor.getColumnIndex("name"))
                contacts.add(contact)
                cursor.moveToNext()
            }
            contacts.forEach {
                info("${it.mobile}")
                //setContact()
            }

            //先移動cursor游標到開頭位置
            cursor.moveToFirst()
            for (i in 0 until cursor.count) {
                //初始化物件
                searchContact = Contacts("", "", "", "", "", "", "")
                //取得資料 拿欄位名稱換index
                searchContact.business_phone = cursor.getString(cursor.getColumnIndex("business_phone"))
                searchContact.combo_name = cursor.getString(cursor.getColumnIndex("combo_name"))
                searchContact.email = cursor.getString(cursor.getColumnIndex("email"))
                searchContact.emp_code = cursor.getString(cursor.getColumnIndex("emp_code"))
                searchContact.mobile = cursor.getString(cursor.getColumnIndex("mobile"))
                searchContact.name = cursor.getString(cursor.getColumnIndex("name"))
                searchContacts.add(searchContact)
                cursor.moveToNext()
            }
            searchContacts.forEach {
                info("${it.email}")
                //setContact()
            }
            cursor.close()
        } else {
            //全體人員的json 搜尋用
            var jsonurl = "http://www.kingbright.com:9999/contact.service"
            doAsync {
                val json = URL(jsonurl).readText()
                contacts = Gson().fromJson<MutableList<Contacts>>(json,
                    object : TypeToken<MutableList<Contacts>>() {}.type
                )
                searchContacts = Gson().fromJson<MutableList<Contacts>>(json,
                    object : TypeToken<MutableList<Contacts>>() {}.type
                )
                contacts.forEach {
                    //info ("${it.name}" )
                    //setContact()
                }
                for (contact in contacts) {
                    //setContact(contact)
                    info("${contact.name}")
                }

                updateDB()

                uiThread {

                    longToast("DB已更新完成")

                }

            }
        }


        /*fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }*/
        //toolbar.setTitle("KB Contact")

        //增加recyclerview預設分割線
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(this)
        //增加recyclerview預設分割線
        recycler.addItemDecoration(itemDecoration)
        recycler.adapter = ContactAdapter(combos)


    }

    private fun updateDB() {
        val helper = ContactHelper(this@MainActivity, "contactlist", null, 1)
        val values = ContentValues()
        //將table 資料清除
        helper.writableDatabase
            .delete("contact", "", null)
        //用迴圈將資料寫入到 table
        for (contact in contacts) {
            values.put("business_phone", contact.business_phone)
            values.put("combo_name", contact.combo_name)
            values.put("email", contact.email)
            values.put("emp_code", contact.emp_code)
            values.put("mobile", contact.mobile)
            values.put("name", contact.name)
            id = helper.writableDatabase
                .insert("contact", null, values)

            info("${contact.emp_code}")
        }

    }
    //onCreate 結束


    //部門用的Adapter
    inner class ContactAdapter(val combos: List<String>) : RecyclerView.Adapter<ContactHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_contact, parent, false)
            return ContactHolder(view)
        }

        override fun getItemCount(): Int {
            return combos.size
        }

        override fun onBindViewHolder(holder: ContactHolder, position: Int) {
            holder.comboText.text = combos.get(position)
            holder.itemView.setOnClickListener {
                functionClicked(combos, position)
            }
        }

    }

    private fun functionClicked(combos: List<String>, position: Int) {
        info(combos.get(position))
        when (combos.get(position)) {
            "國外營業部" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", foreign)
                )
            }
            "國內營業部" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", domestic)
                )
            }
            "管理部" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", management)
                )
            }
            "會計部" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", account)
                )
            }
            "企劃部" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", planning)
                )
            }
            "日本事業部" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", japan)
                )
            }
            "台幹" -> {
                startActivity(
                    Intent(this, ContactListActivity::class.java)
                        .putExtra("combo", tw)
                )
            }
        }
    }

    //部門用的viewholder
    inner class ContactHolder(view: View) : RecyclerView.ViewHolder(view) {
        val comboText: TextView = view.text_combo

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        val searchViewItem = menu.findItem(R.id.action_search2)
        val searchView = searchViewItem.actionView as SearchView
        //searchview 打開時 能佔滿toolbar
        searchView.maxWidth = Int.MAX_VALUE
        searchView.queryHint = "Search View Hint"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            //當搜尋匡內容變動即觸發
            override fun onQueryTextChange(newText: String?): Boolean {


                if (newText!!.isEmpty()) {
                    //搜尋匡內容為空時 顯示原本的部門recyclerview
                    recycler.adapter = ContactAdapter(combos)
                } else {
                    searchContacts.clear()
                    //比對 名稱 分機 郵件 將相符的人員新增到searchContacts 中
                    for (contact in contacts) {
                        if (contact.business_phone.toLowerCase().contains(newText.toLowerCase())) {
                            searchContacts.add(contact)
                        }
                        if (contact.name.toLowerCase().contains(newText.toLowerCase())) {
                            searchContacts.add(contact)
                        }
                        if (contact.email.toLowerCase().contains(newText.toLowerCase())) {
                            searchContacts.add(contact)
                        }
                    }
                    //重新產生搜尋結果的recyclerview
                    recycler.adapter = SearchAdapter(searchContacts)
                }


                return false

            }

        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            //R.id.action_settings -> true
            R.id.action_search2 -> {
                true
            }
            R.id.action_update -> {
                startActivity(Intent(this, UpdateDbActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //搜尋用的 Adapter
    inner class SearchAdapter(val contactApi: MutableList<Contacts>) : RecyclerView.Adapter<SearchHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_contactlist, parent, false)
            return SearchHolder(view)
        }

        override fun getItemCount(): Int {
            return contactApi.size
        }

        override fun onBindViewHolder(holder: SearchHolder, position: Int) {
            holder.nameText.text = contactApi.get(position).name
            //holder.extText.text =contactApi.get(position).business_phone
            //holder.mobileText.text= contactApi.get(position).mobile
            holder.itemView.setOnClickListener {
                functionClicked(contactApi.get(position), position)
            }
        }

    }

    private fun functionClicked(contacts: Contacts, position: Int) {
        startActivityForResult(
            Intent(this, ContactSingleActivity::class.java)
                .putExtra("selectuser", contacts), Activity.RESULT_OK
        )
    }

    //搜尋用的ViewHolder
    inner class SearchHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameText: TextView = view.text_sname
        //val extText: TextView =view.text_ext
        //val mobileText: TextView=view.text_mobile
    }
}

/*interface ContactService {
    @GET("contact.combo")
    fun listContacts(): Call<List<Contacts>>
}*/

fun Activity.setContact(contacts: Contacts) {
    getSharedPreferences("contactdata", Context.MODE_PRIVATE)
        .edit()
        //.putStringSet("contact)
        .putString("name", contacts.name)
        .putString("email", contacts.email)
        .putString("mobile", contacts.mobile)
        .putString("ext", contacts.business_phone)
        .apply()

}
