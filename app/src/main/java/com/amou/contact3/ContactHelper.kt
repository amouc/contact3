package com.amou.contact3

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class ContactHelper(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int) :
    SQLiteOpenHelper(context, name, factory, version) {
    constructor(context: Context?):this(context, database,null, v)

    companion object{
        private  const val  database = "contactlist"
        private  const val  v = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE contact (_id INTEGER PRIMARY KEY NOT NULL,"+
        "business_phone VARCHAR,"+
        "combo_name VARCHAR," +
        "email VARCHAR," +
        "emp_code VARCHAR,"+
        "mobile VARCHAR," +
        "name VARCHAR)")

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

}