package com.amou.contact3

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat

import kotlinx.android.synthetic.main.activity_contact_single.*
import kotlinx.android.synthetic.main.content_contact_single.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class ContactSingleActivity : AppCompatActivity() ,AnkoLogger {

    private val REQUEST_SENDSMS = 900
    private val REQUEST_CALLPHONE = 800
   lateinit var selectUser : Contacts

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_single)
        setSupportActionBar(toolbar)



        text_smobile.setOnClickListener {
            checkPermissionAndCallphone()
        }

        image_mobile.setOnClickListener {
            checkPermissionAndCallphone()
        }

        image_sms.setOnClickListener {
            checkPermissionAndSendsms()
        }

        image_mail.setOnClickListener {
            Sendmail()
        }

        text_smail.setOnClickListener {
            Sendmail()
        }


         //將傳過來的人員物件資料讀進來
         selectUser = intent.getParcelableExtra<Contacts>("selectuser")
        info (selectUser.name)
        //setTitle(selectUser.name.toString())
        text_sname.text = selectUser.name
        text_smobile.text =selectUser.mobile
        text_smail.text = selectUser.email
        text_ext.text = selectUser.business_phone
        //var userEmpcode = selectUser.emp_code.toString()
        //var jsonurl = "http://www.kingbright.com:9999/contact.single?s="+userEmpcode
    }




    //檢查危險權限 允許則發簡訊
    private fun checkPermissionAndSendsms() {
        val permission2 = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.SEND_SMS)
        if(permission2 != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.SEND_SMS),REQUEST_SENDSMS)
        }else{
            sendSms()
        }

    }



    //檢查危險權限 允許則撥打電話
    private fun checkPermissionAndCallphone() {
        val permission = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.CALL_PHONE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CALLPHONE)
        } else {
            callPhone()
        }
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            REQUEST_CALLPHONE -> {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    callPhone()
                }else{

                }
            }

            REQUEST_SENDSMS ->{
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    sendSms()
                }else{

                }
            }

        }
    }

    private fun Sendmail() {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:${selectUser.email}")
            //putExtra(Intent.EXTRA_EMAIL, selectUser.email)
        }
        //if(intent.resolveActivity(packageManager)!= null){
            startActivity(intent)
        //}

    }

    private fun sendSms() {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("smsto:${selectUser.mobile}")
            //putExtra("sms_body","test message")
        }

        //if(intent.resolveActivity(packageManager)!= null){
            startActivity(intent)
        //}


    }

    private fun callPhone() {
        val intent = Intent(Intent.ACTION_CALL)
        val data = Uri.parse("tel:${selectUser.mobile}")
        intent.data = data
        startActivity(intent)
    }

}
